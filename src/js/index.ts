/**
*   Libraries
 */
import * as $ from 'jquery';
import 'bootstrap';
import "flickity";
var flickity = require('flickity');
import 'flickity-fullscreen';

/**
 * Styles
 */
import '../scss/index.scss';

/**
 * Importa aquí los modulos
 */




/**
 * Global Javascript
 */
import { custom } from './custom';

/**
 * Init
 */
custom(flickity);

